<?php

namespace RetailCrm;

use Doctrine\Common\Cache\CacheProvider;

class utmPromoGen
{
    /**
     * Генерация ключей по кругу, т.е. если закончатся все возможные варианты генерации ключей, то значения ключей будут
     * перезаписываться с самого первого
     * По умолчанию.
     */
    const GEN_ROUND_ROBIN = 1;
    /**
     * Если закончатся все возможные варианты генерации ключей, то будет выброшено исключение.
     */
    const GEN_EXCEPTION = 2;

    /**
     * @var string Имя файла, в котором сохраняются данные о значениях utm-меток и сгенерированных для них кодов
     */
    protected $fileName;
    /**
     * @var string Список символов, из которых генерится промокод.
     *             Глобальная настройка, может быть переопределена в utmConfig
     */
    protected $charsList = '123456789АБВГДЕЖЗИКЛМНПРСТУФШЭЮЯ';
    /**
     * @var int Число символов в промокоде, генерируемое для каждой utm-метки
     *          Глобальная настройка, может быть переопределена в utmConfig
     */
    protected $charsCount = 2;
    /**
     * @var array Список utm-меток, которые используются при генерации промокода. Также определяет порядок, в котором
     *            будут генерироваться коды в методе getKeyFromArray
     */
    protected $utmLabels = array('utm_source', 'utm_medium', 'utm_campaign');
    /**
     * @var array Настройки генератора для определённых utm-меток. Здесь можно переопределить общие для всех настройки
     *            Например:
     *            array(
     *              'utm_source' => array(
     *                'charsCount' => 2,           // число генерируемых символов для промокода
     *                'charsList' => '123456789'   // список символов для генерации
     *              ),
     *            )
     */
    protected $utmConfig = array();
    /**
     * @var int Тип генерации ключей
     */
    protected $type = self::GEN_ROUND_ROBIN;
    /**
     * @var CacheProvider
     */
    protected $cacheProvider = null;
    protected $utmData = array();
    protected $fh = null;

    /**
     * @param array $config Массив конфигурации
     *                      Доступные опции:
     *
     * filename - строка, имя файла для сохранения данных о задействованных промокодах и из значений utm-меток
     * charsList
     * charsCount
     * utmLabels
     * utmConfig
     * type
     * cache
     */
    public function __construct($config = array())
    {
        \mb_internal_encoding('utf-8');

        $this->fileName = isset($config['filename']) ? $config['filename'] : 'utm_data';
        if (isset($config['charsList'])) {
            $this->charsList = $config['charsList'];
        }
        if (isset($config['charsCount'])) {
            $this->charsCount = $config['charsCount'];
        }
        if (isset($config['utmLabels'])) {
            $this->utmLabels = $config['utmLabels'];
        }
        if (isset($config['utmConfig'])) {
            $this->utmConfig = $config['utmConfig'];
        }
        if (isset($config['type'])) {
            $this->type = $config['type'];
        }

        if (isset($config['cache']) && $config['cache'] instanceof CacheProvider) {
            $this->cacheProvider = $config['cache'];
        }

        // заполним $this->utmConfig данными для всех указанных utm-меток
        foreach ($this->utmLabels as $utm) {
            if (!isset($this->utmConfig[$utm])) {
                $this->utmConfig[$utm] = array();
            }
            if (!isset($this->utmConfig[$utm]['charsCount'])) {
                $this->utmConfig[$utm]['charsCount'] = $this->charsCount;
            }
            if (!isset($this->utmConfig[$utm]['charsList'])) {
                $this->utmConfig[$utm]['charsList'] = $this->charsList;
            }
        }
    }

    /**
     * Возвращает ключ, соответствующий заданной utm-метке и значению. Если такого значения нет, то оно будет добавлено
     * с новым ключом, и этот ключ будет возвращён.
     * Зависит от типа генерации ключей.
     *
     * @param string $utmLabel utm-метка
     * @param string $value
     *
     * @return mixed|string
     *
     * @throws \Exception
     */
    public function getKeyForValue($utmLabel, $value)
    {
        if (!in_array($utmLabel, $this->utmLabels)) {
            return false;
        }

        $this->load();

        $key = array_search($value, $this->utmData['utm'][$utmLabel]);
        if ($key !== false) {
            $this->close(false);

            return $key;
        }
        $key = $this->generateUtmKey($utmLabel);

        if ($key === false) {
            switch ($this->type) {
                case self::GEN_ROUND_ROBIN:
                    // если возможные варианты генерации ключей закончились, делаем перезапись ключей с начала
                    $keys = array_keys($this->utmData['utm'][$utmLabel]);
                    $key = isset($this->utmData['rewrite'][$utmLabel]) ? $this->utmData['rewrite'][$utmLabel] : '';
                    if (!$key) {
                        $key = $keys[0];
                    } else {
                        $num = array_search($key, $keys);
                        if ($num < count($keys) - 1) {
                            $key = $keys[$num + 1];
                        } else {
                            $key = $keys[0];
                        }
                    }
                    $this->rewrite($utmLabel, $key, $value);
                    $this->close();

                    return $key;
                case self::GEN_EXCEPTION:
                    throw new \Exception('Закончились варианты генерации ключей для "'.$utmLabel.'"');
            }
        }

        $this->set($utmLabel, $key, $value);
        $this->close();

        return $key;
    }

    /**
     * Возвращает ключ, исходя из переданного массива. Удобно использовать для передачи в функцию GET-параметров запроса.
     *
     * @param $data
     *
     * @return string
     *
     * @throws \Exception
     */
    public function getKeyFromArray($data)
    {
        $key = '';
        foreach ($this->utmLabels as $utmLabel) {
            if (isset($data[$utmLabel])) {
                $key .= $this->getKeyForValue($utmLabel, $data[$utmLabel]);
            }
        }

        return $key;
    }

    /**
     * Возвращает значение, соответствующее указанной utm-метке и ключу.
     *
     * @param string $utmLabel
     * @param string $key
     *
     * @return bool
     */
    public function getValueForKey($utmLabel, $key)
    {
        $this->load();
        $this->close(false);

        return isset($this->utmData['utm'][$utmLabel][$key]) ? $this->utmData['utm'][$utmLabel][$key] : false;
    }

    protected function init()
    {
        $this->utmData = array();
        foreach ($this->utmConfig as $utm => $utmConfig) {
            $this->set($utm, $this->generateUtmKey($utm), '');
        }
    }

    protected function load()
    {
        if ($this->fh && !$this->cacheProvider) {
            $this->close(false);
        }
        if ($this->cacheProvider) {
            $this->utmData = $this->cacheProvider->fetch('utmPromoGenData');
        } else {
            clearstatcache(false, $this->fileName);
            $this->fh = fopen($this->fileName, 'c+');
            flock($this->fh, LOCK_EX);
            $str = ($filesize = filesize($this->fileName)) ? fread($this->fh, $filesize) : '';
            $this->utmData = $str ? json_decode($str, true) : array();
        }

        if (!$this->utmData) {
            $this->init();
        }
    }

    protected function close($save = true)
    {
        if (!$this->fh && !$this->cacheProvider) {
            if (!$save) {
                return;
            }
            $this->fh = fopen($this->fileName, 'c+');
            flock($this->fh, LOCK_EX);
        }
        if ($save) {
            if ($this->cacheProvider) {
                $this->cacheProvider->save('utmPromoGenData', $this->utmData);
            } else {
                rewind($this->fh);
                $str = json_encode($this->utmData);
                $length = mb_strlen($str);
                fwrite($this->fh, $str, $length);
                ftruncate($this->fh, $length);
                fflush($this->fh);
            }
        }
        if (!$this->cacheProvider) {
            flock($this->fh, LOCK_UN);
            fclose($this->fh);
            $this->fh = null;
        }
    }

    protected function set($utmLabel, $key, $value)
    {
        $this->utmData['utm'][$utmLabel][$key] = $value;
    }

    protected function rewrite($utmLabel, $key, $value)
    {
        // логирование
        $msg = date('Y-m-d H:i:s')."\tПерезапись ключа '$key' для $utmLabel: старое значение '{$this->utmData['utm'][$utmLabel][$key]}', новое значение '$value'\n";
        file_put_contents(dirname(__FILE__).DIRECTORY_SEPARATOR.basename(__FILE__).'.log', $msg, FILE_APPEND);
        //
        $this->utmData['utm'][$utmLabel][$key] = $value;
        $this->utmData['rewrite'][$utmLabel] = $key;
    }

    /**
     * @param string $utmLabel utm-метка
     *
     * @return string Сгенерированный ключ
     */
    protected function generateUtmKey($utmLabel)
    {
        $charsList = $this->utmConfig[$utmLabel]['charsList'];
        $charsCount = $this->utmConfig[$utmLabel]['charsCount'];

        $arr = isset($this->utmData['utm'][$utmLabel]) ? array_keys($this->utmData['utm'][$utmLabel]) : array('');
        $last = array_pop($arr);
        if ($last === '') {
            return str_repeat($charsList[0], $charsCount);
        }
        $lastArr = array();
        for ($i = 0; $i < mb_strlen($last); $i++) {
            $lastArr[] = mb_substr($last, $i, 1);
        }

        $res = $this->getNext($charsList, $lastArr, count($lastArr) - 1);
        if ($res) {
            return implode('', $lastArr);
        }

        return false;
    }

    /**
     * Поиск следующиего возможного значения для указанного элемента массива.
     *
     * @param array $charsList
     * @param array $arr
     * @param int   $num
     *
     * @return bool
     */
    protected function getNext($charsList, &$arr, $num)
    {
        if ($num < 0) {
            return false;
        }
        $s = $this->getNextSymbol($charsList, $arr[$num]);
        if ($s) {
            $arr[$num] = $s;

            return true;
        }
        $arr[$num] = $this->getNextSymbol($charsList);

        return $this->getNext($charsList, $arr, $num - 1);
    }

    /**
     * Поиск значения, следующего за $char, из списка возможных
     * Если передан null, то возвращается первый элемент списка
     * Если переданное значение не найдено, или оно последнее в списке, возвращается false.
     *
     * @param array       $charsList
     * @param string|null $char
     *
     * @return bool|string
     */
    protected function getNextSymbol($charsList, $char = null)
    {
        if ($char === null) {
            return mb_substr($charsList, 0, 1);
        }
        $pos = mb_strpos($charsList, $char);
        if ($pos === false || $pos == (mb_strlen($charsList) - 1)) {
            return false;
        }

        return mb_substr($charsList, $pos + 1, 1);
    }
}
